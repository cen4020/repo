﻿/*
 * At the start of the program this code will allow the user to pass through
 * the menu options with the arrow keys. The first botton will be selected 
 * by default
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectOnInput : MonoBehaviour {
    public EventSystem eventSystem;
    public GameObject selectedObject;

    private bool button;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {                                                //coded by Leah and Daniel
        if (Input.GetAxisRaw("Vertical")!= 0 && button == false)
        {
            eventSystem.SetSelectedGameObject(selectedObject);
            button = true;
        }
	}

    private void OnDisable()
    {
        button = false;
    }
}
