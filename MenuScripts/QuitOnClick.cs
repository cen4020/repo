﻿/*
 when this code is executed the user will exit out eof the program
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitOnClick : MonoBehaviour {

	public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;	//coded by Daniel and Steven
#else
        Application.Quit():
#endif
    }
}
