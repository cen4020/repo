using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class glbl : MonoBehaviour {

	/*Items (esp. key) declarations here --
	One key per locked door, where all but the first key are behind locked doors.
	Ending room requires a final key.*/
	
	public Doors lastdoor; /*Save the door generated in the last for-loop step -- use it to line up the next room*/
	
	public Transform starter;
	public Transform ender;
    public Transform prefab;
    void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            Instantiate(prefab, new Vector3(Random.Range(-100, 100), 0, Random.Range(-100, 100)));
			lastdoor = prefab.Doors;
        }
    }

}
