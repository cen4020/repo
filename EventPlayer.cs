using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;

namespace UnityStandardAssets.Characters.FirstPerson{
[RequireComponent(typeof (AudioSource))]
public class EventPlayer : MonoBehaviour {
	
	[SerializeField] private AudioClip[] m_EventSounds;
	private AudioSource m_AudioSource;
	
	// Use this for initialization
	void Start () {
		m_AudioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Random.Range(1, 1000)==1){
			int n = Random.Range(1, m_EventSounds.Length);
            m_AudioSource.clip = m_EventSounds[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
		}
	}
}
}